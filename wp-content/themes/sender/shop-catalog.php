<?php
/**
 * Template Name: Shop Template
 *
 * A custom shop page template.
 *
*/

get_header(); ?>
<div class="content shop">
			<?php
									$postIDs = array();
									
									$parentstop = get_post_ancestors( $post->ID );
									$idparent = ($parentstop) ? $parentstop[count($parentstop)-1]: $post->ID;
									
									$pageChildren = get_pages(
									array (
									'child_of' => $idparent,
									'include' => $gen3_ids
									)
								);
	
						if ( $pageChildren ) {
						  foreach ( $pageChildren as $pageChild ) {
							$postIDs[] = $pageChild->ID;
						  }
						  $args = array(
							'post_type' => 'page',
							'post__in' => $postIDs,
							'orderby'  => 'post_date',
							'order' => 'DESC',	
							'meta_key' => 'slider_featured',
							'meta_value' => 'yes',				
							);
						  query_posts($args);
										  
							if ( have_posts() ) :?>
							<div class="slider-shop">
							<div id="featured-navi">
								<div id="nav-featured" class="bubbles-nav"></div>
									<div id="featured">
										<?php while ( have_posts() ) : the_post();?>
											<div class="item">
													<div class="shop-slider-info">
														<div class="slider-price">
															<?php if(get_field('price'))
																{
																	echo '<div class="price"><span class="number">'. get_field('price') . '</span><span class="money-type">$</span></div>';
																}
															?>
														</div>
														<div class="parent-tag">
															<?php if ( get_post_meta($post->post_parent, 'icon_parent_tag', true) ) : ?>
																<?php $image_icon = wp_get_attachment_image_src (get_post_meta($post->post_parent, 'icon_parent_tag', true)); ?>
																<img src="<?php echo $image_icon[0]; ?>" />
															<?php endif; ?>
															<?php echo get_the_title($post->post_parent); ?>									
														</div>
														
														<a href="<?php the_permalink(); ?>">
															<h3><?php the_title(); ?></h3>
														</a>
														<?php the_excerpt(); ?>
														</div>
														<div class="slider-buy-button">
															<?php if ( qtrans_getLanguage() == 'en' ) {
																if(get_field('button_buy'))
																	{
																		echo '<div class="buy-button"><a href="' . get_field('button_buy') . '" target="_blank">' . get_field('button_eng') . '</a></div>';
																	}
																
															} else {
																if(get_field('button_buy'))
																	{
																		echo '<div class="buy-button"><a href="' . get_field('button_buy') . '" target="_blank">' . get_field('button_rus') . '</a></div>';
																	}
															}
															?>
														</div>
													<div class="shop-slider-image"><?php echo '<img src="' .get_field('image_slider'). '" />'; ?></div>
										</div>
										
								<?php endwhile;?>
								</div>
							</div>
						</div>
						
						<?php
							else :
								echo wpautop( '' );
							endif;
							?>												
				<?php }?>
				<?php wp_reset_query(); ?>
	<div class="shop-tags">
		<!-- Start Code -->
					<?php
							$postIDs = array();
							$pageChildren = get_pages(
							array (
							'child_of' => $post->post_parent,
							'parent' => $post->post_parent,
							)
						);
	
						if ( $pageChildren ) {
						  foreach ( $pageChildren as $pageChild ) {
							$postIDs[] = $pageChild->ID;
						  }
						  $args = array(
							'post_type' => 'page',
							'post__in' => $postIDs,
							'orderby'  => 'post_date',
							'order' => 'DESC',
							'posts_per_page' => 10,						
							);
						  query_posts($args);
						  
				if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="shop-tag">
						<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</div>					  
				<?php endwhile; ?>
				<?php endif; ?>
				<div class="fix"></div>
			<?php }?>
		<?php wp_reset_query(); ?>
		<!-- End Code -->	
	</div>	
			<!-- Start Code -->
			        <ul>
					<?php
							$postIDs = array();
							$pageChildren = get_pages(
							array (
							'child_of' => $post->ID,
							'parent' => $post->ID,
							)
						);
				
						if ( $pageChildren ) {
							foreach ( $pageChildren as $pageChild ) {
							$postIDs[] = $pageChild->ID;
						}
						$args = array(
							'post_type' => 'page',
							'post__in' => $postIDs,
							'orderby'  => 'post_date',
							'order' => 'DESC',
                            'meta_query' => array(
                                array(
                                    'key' => '_wp_page_template',
                                    'value' => 'shop-item.php', /* your template file name */
                                    'compare' => '='
                                )
                            )
						);
						query_posts($args);
						
						  if (have_posts()) : while (have_posts()) : the_post(); ?>
								<li class="shop-content">
									<div class="parent-tag">
											<?php if ( get_post_meta($post->post_parent, 'icon_parent_tag', true) ) : ?>
												<?php $image_icon = wp_get_attachment_image_src (get_post_meta($post->post_parent, 'icon_parent_tag', true)); ?>
												<img src="<?php echo $image_icon[0]; ?>" />
											<?php endif; ?>
											<?php echo get_the_title($post->post_parent); ?> 
											<?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?>										
									</div>
									<a href="<?php the_permalink(); ?>" class="shop-link">
									<div class="shop-thumb">
										<div class="bottom">
												<?php if ( qtrans_getLanguage() == 'en' ) {
													if(get_field('button_buy'))
														{
															echo '<div class="buy-button">' . get_field('button_eng') . '</div>';
														}
													
												} else {
													if(get_field('button_buy'))
														{
															echo '<div class="buy-button">' . get_field('button_rus') . '</div>';
														}
												}
												?>
										</div>
										<div class="top"><?php the_post_thumbnail('thumbnail'); ?></div>
									</div>
										<h3><?php the_title(); ?></h3>
									</a>
									<div class="fix"></div>

									<?php if(get_field('price'))
										{
											echo '<div class="price"><span class="number">'. get_field('price') . '</span><span class="money-type">$</span></div>';
										}
									?>
								</li>					  
						  <?php endwhile; ?>
						
						<?php endif; ?>
						<div class="fix"></div>
						<?php wp_simple_pagination(); ?>
						
						<?php }?>
						<?php wp_reset_query(); ?>
						</ul>
					<!-- End Code -->
<?php get_footer(); ?>