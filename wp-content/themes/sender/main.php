<?php 
/**
 * Template Name: Main Page Template
 *
 * A custom Main Page template.
 *
*/

get_header(); ?>
		<div class="content main">
		<?php do_shortcode('[upzslider usingphp=true]'); ?>
			<?php
			$paged = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
			$title = get_the_title($post->ID);
			$args= array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'paged' => $paged,
			);
			query_posts($args);?>
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div class="blog-item" id="<?php the_ID(); ?>">
				<div class="blog-date"><?php the_time('j F Y'); ?><?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?></div>
				<?php if(get_field('post_video_thumb'))
								{
									echo '<div class="blog-thumb-video">' . get_field('post_video_thumb') . '</div>';
								}
				else { ?>
				<div class="blog-img"><a class="blog-thumb" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_post_thumbnail('thumbnail'); ?></a></div>
				<?php } ?>
				<a class="blog-title" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><h1><strong><?php the_title(); ?></strong></h1></a>
				<div class="blog-content">
					<?php the_advanced_excerpt('length=30&exclude_tags=img&allowed_tags=p,strong'); ?>
					<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
				</div><!-- .entry-content -->	
				<div class="blog-comments"><span><?php comments_number('0', '1', '%'); ?></span></div>
			</div>
		<?php endwhile; // end of the loop. ?>
		<div class="fix"></div>
		<div class="blog button"><a href="/blog">
			<?php if ( qtrans_getLanguage() == 'en' ) { ?>
				Read the blog
				<?php } else { ?>
				Читать на блоге
			<?php } ?>
		</a></div>
		
		</div><!-- .entry-content -->
				
<?php get_footer(); ?>