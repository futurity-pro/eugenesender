<?php

/**

* A single event.  This displays the event title, description, meta, and 

* optionally, the Google map for the event.

*

* You can customize this view by putting a replacement file of the same name (single.php) in the events/ directory of your theme.

*/



// Don't load directly

if ( !defined('ABSPATH') ) { die('-1'); }

?>
<div class="right" style="padding-right:60px;">
		<div class="page-title">
			<h2 style="margin:0; padding:0;"><?php if ( qtrans_getLanguage() == 'en' ) { ?>
				Future Events
				<?php } else { ?>
				Предстоящие события
			<?php } ?></h2>
		</div>
		<?php global $post;
				$args = array(
					'post_type' => 'tribe_events',
					'eventDisplay' => 'upcoming',
					'orderby'  => 'post_date',
					'posts_per_page' => 5,
					'post__not_in' => array($post->ID),
					'order' => 'DESC',
				);
				query_posts($args);
				if (have_posts()) : while (have_posts()) : the_post();?>
				
				<div class="event-item" style="float:none;">
				<div class="event-title-top">	
					<?php if(get_field('category_icon'))
						{
							echo '<div class="category_icon"><img src="' . get_field('category_icon') . '"/></div>';
						}
					?>
					<div class="event_day"><?php echo tribe_get_start_date( null, false ); ?></div>
					<?php if ( qtrans_getLanguage() == 'en' ) {								
						if(get_field('event_cat_en'))
							{
								echo '<div class="event-cat">/ ' . do_shortcode(get_field('event_cat_en')) . '</div>';
							}
							} else {
								if(get_field('event_cat_ru'))
									{
										echo '<div class="event-cat">/ ' . do_shortcode(get_field('event_cat_ru')) . '</div>';
									}
							}?>
				</div>
			
				<?php if(has_post_thumbnail($post->ID, 'thumbnail')):
				$image_id = get_post_thumbnail_id($post->ID);
				$image_url = wp_get_attachment_image_src($image_id, 'thumbnail', true); ?>
				<a href="<?php echo get_permalink($post->ID) ?>" class="post-thumb"><img src="<?php echo $image_url[0]; ?>" alt="<?php echo $post->post_title ?>" /></a>
				<?php endif; ?>
				
				<div class="event-title-link">
					<a href="<?php echo get_permalink($post->ID) ?>">
						<h3><strong><?php echo $post->post_title; ?></strong></h3>
					</a>
				</div>
				
				<div class="event-adress">
				<?php _e( tribe_get_venue( get_the_ID() )); ?><?php _e(',&nbsp;'. tribe_get_address( get_the_ID() )); ?><?php _e(',&nbsp;' .tribe_get_city( get_the_ID() )); ?><?php _e(',&nbsp;' .tribe_get_country( get_the_ID() )); ?>
				</div>
				
				<div class="fix"></div>
			
				<?php the_excerpt(); ?>
				
				<div class="links-events">
					<?php if ( qtrans_getLanguage() == 'en' ) {								
						if(get_field('buy_ticket_en'))
							{
								echo '<div class="buy_ticket"><a href="' . do_shortcode(get_field('buy_ticket_event')) . '">' . do_shortcode(get_field('buy_ticket_en')) . '</a></div>';
							}
							} else {
								if(get_field('buy_ticket_ru'))
									{
										echo '<div class="buy_ticket"><a href="' . do_shortcode(get_field('buy_ticket_event')) . '">' . do_shortcode(get_field('buy_ticket_ru')) . '</a></div>';
									}
					}?>
					
					<?php if(get_field('vk_event'))
							{
								echo '<div class="vk_event"><a href="' . do_shortcode(get_field('vk_event')) . '"><img src="/wp-content/themes/sender/images/vk-icon-event.png" /></a></div>';
							}
					?>
					
					<?php if(get_field('fb_event'))
							{
								echo '<div class="fb_event"><a href="' . do_shortcode(get_field('fb_event')) . '"><img src="/wp-content/themes/sender/images/facebook-icon-event.png" /></a></div>';
							}
					?>
				</div>
				<div class="fix"></div>
				<div class="blog-comments"><span><?php comments_number('0', '1', '%'); ?></span></div>
			</div>	
		<?php endwhile;?>
		<?php endif;?>
		<div class="fix" style="margin:40px 0;"></div>
		
		<div class="all-archive"><a href="<?php echo tribe_get_events_link(); ?>"><?php if ( qtrans_getLanguage() == 'en' ) { ?>
								All Events
								<?php } else { ?>
								Все события
							<?php } ?></a></div>
		<?php wp_reset_query(); ?>
</div>

<div class="left">
	<?php
		if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) {?>
			<?php the_post_thumbnail('large'); ?>
	<?php } ?>
	
	<div class="event-title-top" style="margin-top:25px;">	
					<?php if(get_field('category_icon'))
						{
							echo '<div class="category_icon"><img src="' . get_field('category_icon') . '"/></div>';
						}
					?>
					<div class="event_day"><?php echo tribe_get_start_date( null, false ); ?></div>
					<?php if ( qtrans_getLanguage() == 'en' ) {								
						if(get_field('event_cat_en'))
							{
								echo '<div class="event-cat">/ ' . do_shortcode(get_field('event_cat_en')) . '</div>';
							}
							} else {
								if(get_field('event_cat_ru'))
									{
										echo '<div class="event-cat">/ ' . do_shortcode(get_field('event_cat_ru')) . '</div>';
									}
							}?>
	</div>
	
	<h1 class="blog-title single"><strong><?php the_title(); ?></strong></h1>
	<div class="event-adress">
		<?php _e( tribe_get_venue( get_the_ID() )); ?><?php _e(',&nbsp;'. tribe_get_address( get_the_ID() )); ?><?php _e(',&nbsp;' .tribe_get_city( get_the_ID() )); ?><?php _e(',&nbsp;' .tribe_get_country( get_the_ID() )); ?>
	</div>
	
	<div class="entry-content" style="margin:25px 0">
		<?php the_content(); ?>
	</div>
	
	<?php if( tribe_embed_google_map( get_the_ID() ) ) : ?>
	
	<?php if( tribe_address_exists( get_the_ID() ) ) { echo tribe_get_embedded_map(); } ?>
	
	<?php endif; ?>
	
	<div class="links-events event-link-content" style="border-top:1px solid #E5E5E5; padding-top:20px;">
	
	<?php if (strtotime( tribe_get_end_date(get_the_ID(), false, 'Y-m-d G:i') . $gmt_offset ) <= time() ) { ?>
		<?php if ( qtrans_getLanguage() == 'en' ) {								
				if(get_field('photo_events'))
					{
						echo '<div class="photo_events"><a href="' . do_shortcode(get_field('photo_events')) . '">Photo Gallery</a></div>';
					}
					} else {
						if(get_field('photo_events'))
							{
								echo '<div class="photo_events"><a href="' . do_shortcode(get_field('photo_events')) . '">Фотоотчет</a></div>';
							}
			} ?>
	<?php } else {
	
		 if ( qtrans_getLanguage() == 'en' ) {								
						if(get_field('buy_ticket_en'))
							{
								echo '<div class="buy_ticket"><a href="' . do_shortcode(get_field('buy_ticket_event')) . '">' . do_shortcode(get_field('buy_ticket_en')) . '</a></div>';
							}
							} else {
								if(get_field('buy_ticket_ru'))
									{
										echo '<div class="buy_ticket"><a href="' . do_shortcode(get_field('buy_ticket_event')) . '">' . do_shortcode(get_field('buy_ticket_ru')) . '</a></div>';
									}
					}						
	}
	?>
			<?php if(get_field('vk_event'))
							{
								echo '<div class="vk_event"><a href="' . do_shortcode(get_field('vk_event')) . '"><img src="/wp-content/themes/sender/images/vk-icon-event.png" /></a></div>';
							}
					?>
					
			<?php if(get_field('fb_event'))
							{
								echo '<div class="fb_event"><a href="' . do_shortcode(get_field('fb_event')) . '"><img src="/wp-content/themes/sender/images/facebook-icon-event.png" /></a></div>';
							}	?>
	</div>
	<div class="fix"></div>
	
	<div class="event-comment-tabber">
		<?php if(tribe_get_option('showComments','no') == 'yes'){ comments_template(); } ?>
	</div>
</div>