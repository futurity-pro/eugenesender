<?php
/**
 * Template Name: Media Catalog Template
 *
 * A custom media catalog page template.
 *
*/

get_header(); ?>
<div class="content shop">
<div class="page-title">
	<h1><font style="color:#666666;"><?php echo get_the_title($post->post_parent);?></font> &gt; <?php the_title(); ?></h1>
</div>

	<div class="shop-tags" style="margin: 0 0 30px;">
		<!-- Start Code -->
					<?php
							$postIDs = array();
							$pageChildren = get_pages(
							array (
							'child_of' => $post->post_parent,
							'parent' => $post->post_parent,
							)
						);
	
						if ( $pageChildren ) {
						  foreach ( $pageChildren as $pageChild ) {
							$postIDs[] = $pageChild->ID;
						  }
						  $args = array(
							'post_type' => 'page',
							'post__in' => $postIDs,
							'orderby'  => 'post_date',
							'order' => 'DESC',
							'posts_per_page' => 10,						
							);
						  query_posts($args);
						  
				if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="shop-tag">
						<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</div>					  
				<?php endwhile; ?>
				<?php endif; ?>
				<div class="fix"></div>
			<?php }?>
		<?php wp_reset_query(); ?>
		<!-- End Code -->	
	</div>	
			<!-- Start Code -->
			    <ul>
					<?php
							$postIDs = array();
							$pageChildren = get_pages(
							array (
							'child_of' => $post->ID,
							'parent' => $post->ID,
							)
						);
				
						if ( $pageChildren ) {
							foreach ( $pageChildren as $pageChild ) {
							$postIDs[] = $pageChild->ID;
						}
						$args = array(
							'post_type' => 'page',
							'post__in' => $postIDs,
							'orderby'  => 'post_date',
							'order' => 'DESC',
                            'meta_query' => array(
                                array(
                                    'key' => '_wp_page_template',
                                    'value' => 'media-item.php', /* your template file name */
                                    'compare' => '='
                                )
                            )
						);
						query_posts($args);
						
						  if (have_posts()) : while (have_posts()) : the_post(); ?>
								<li class="shop-content">
									<div class="parent-tag">
											<?php if ( get_post_meta($post->post_parent, 'icon_parent_tag', true) ) : ?>
												<?php $image_icon = wp_get_attachment_image_src (get_post_meta($post->post_parent, 'icon_parent_tag', true)); ?>
												<img src="<?php echo $image_icon[0]; ?>" />
											<?php endif; ?>
											<?php echo get_the_title($post->post_parent); ?> 
											<?php if(get_the_time('U') > time() - 1*24*3000) echo '<span>new</span>'; ?>										
									</div>
									<div class="shop-thumb media">
										<?php if(get_field('post_video_thumb'))
														{
															echo '<div class="blog-thumb-video">' . get_field('post_video_thumb') . '</div>';
														}
										else { ?>
										<div class="blog-img"><a class="blog-thumb" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_post_thumbnail('thumbnail'); ?></a></div>
										<?php } ?>
									</div>
									
									<a href="<?php the_permalink(); ?>">
										<h3><?php the_title(); ?></h3>
									</a>
									<div class="fix"></div>

									<div class="blog-comments"><span><?php comments_number('0', '1', '%'); ?></span></div>
								</li>					  
						  <?php endwhile; ?>
						
						<?php endif; ?>
						<div class="fix"></div>
						<?php wp_simple_pagination(); ?>
						
						<?php }?>
						<?php wp_reset_query(); ?>
						</ul>
					<!-- End Code -->
<?php get_footer(); ?>