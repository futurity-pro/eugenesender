<?php
/**
 * Template Name: Events Template
 *
 * A custom events page template.
 *
*/

if ( !defined('ABSPATH') ) { die('-1'); }
get_header();
?>
<div class="content shop">
<?php tribe_events_before_html(); ?>
<?php include(tribe_get_current_template()); ?>
<?php tribe_events_after_html(); ?>
<div class="fix"></div>
</div><!-- .entry-content -->
<?php get_footer(); ?>