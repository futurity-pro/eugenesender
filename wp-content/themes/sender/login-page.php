<?php
/**
 * Template Name: Login Page Template
 *
 *
*/

get_header(); ?>
<div class="content login">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div class="page-title"><h1><?php the_title();?></h1></div>			
				<?php the_content(); ?>
			<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
		<?php endwhile; // end of the loop. ?>
		<div class="fix"></div>
		</div><!-- .entry-content -->
				
<?php get_footer(); ?>